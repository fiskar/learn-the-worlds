import React, { Component } from 'react';
import HomePage from './pages/Home';
import LoginPage from './pages/Login';
import { Spin, Layout, Menu } from 'antd';
import s from './App.module.scss';
import FirebaseContext from './context/firebaseContext';
import { Route, Link, Switch } from 'react-router-dom';
import RegisterPage from './pages/Register';
import { PrivateRoute } from './utils/privateRoutes';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addUserAction } from './actions/userAction';

const { Header, Content } = Layout;

class App extends Component {
    state = {
        user: null,
    };
    componentDidMount() {
        const { auth, setUserUid } = this.context,
            { addUser } = this.props;
        auth.onAuthStateChanged((user) => {
            if (user) {
                setUserUid(user.uid);
                localStorage.setItem('user', JSON.stringify(user.uid));
                addUser(user);
                this.setState({
                    user,
                });
            } else {
                setUserUid(null);
                localStorage.removeItem('user');
                this.setState({
                    user: false,
                });
            }
        });
    }

    render() {
        const { user } = this.state;
            // { userUid } = this.props;

        if (user === null) {
            return (
                <div className={s.loader_wrap}>
                    <Spin size="large" />
                </div>
            );
        }

        return (
            <Switch>
                <Route path="/login" component={LoginPage} />
                <Route
                    render={() => {
                        return (
                            <Layout>
                                <Header>
                                    <Menu
                                        theme="dark"
                                        mode="horizontal"
                                        defaultSelectedKeys={[' ']}
                                    >
                                        <Menu.Item key="1">
                                            <Link to="/">Home</Link>
                                        </Menu.Item>
                                        <Menu.Item key="2">
                                            <Link to="/register">Register</Link>
                                        </Menu.Item>
                                    </Menu>
                                </Header>
                                <Content>
                                    <PrivateRoute
                                        path="/"
                                        exact
                                        component={HomePage}
                                    />
                                    <PrivateRoute
                                        path="/home"
                                        component={HomePage}
                                    />
                                    <PrivateRoute
                                        path="/register"
                                        component={RegisterPage}
                                    />
                                </Content>
                            </Layout>
                        );
                    }}
                />
            </Switch>
        );
    }
}
App.contextType = FirebaseContext;

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            addUser: addUserAction,
        },
        dispatch
    );
};
const mapStateToProps = (state) => {
    return {
        userUid: state.user.userUid,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
