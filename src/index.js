import React from 'react';
import ReactDom from 'react-dom';
import { createStore, applyMiddleware } from 'redux';

import App from './App';
import rootReducer from './reducers';

import 'antd/dist/antd.css';
import './index.css';
import FirebaseContext from './context/firebaseContext';
import Firebase from './servises/firebase';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

const store = new createStore(rootReducer, applyMiddleware(thunk, logger));

ReactDom.render(
    <Provider store={store}>
        <FirebaseContext.Provider value={new Firebase()}>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </FirebaseContext.Provider>
    </Provider>,
    document.getElementById('root')
);
