import React, { Component } from "react";
import Card from "../Card";
import s from "./CardList.module.scss";
import FirebaseContext from "../../context/firebaseContext";

class CardList extends Component {
	state = {
		card: this.createCard(),
		showRusPreview: null,
	};

	createCard() {
		return {
			eng: "",
			rus: "",
			id: null,
			isEmpty: function () {
				return !this.eng && !this.rus;
			},
		};
	}

	createCardForm(form) {
		const card = this.createCard();
		card.eng = form.eng.value;
		card.rus = form.rus.value;
		return card;
	}

	handleCardInputChange = (e) => {
		let card = this.createCardForm(e.target.form),
			showRusPreview = "rus" === e.target.rus;

		this.setState(() => {
			return {
				card: card,
				showRusPreview: showRusPreview,
			};
		});
	};

	handleCardSubmitForm = (e) => {
		e.preventDefault();
		let card = this.createCardForm(e.target);
		if (!card.isEmpty()) {
			this.props.onAddCard(card);
		}
		this.setState(() => {
			return {
				card: this.createCard(),
				showRusPreview: null,
			};
		});
	};

	render() {
		let { item, onDeletedItem } = this.props;
		const { card, showRusPreview } = this.state;
		return (
			<>
				<div className={s.preview}>
					{!card.isEmpty() && (
						<Card
							eng={card.eng}
							rus={card.rus}
							shoAsDone={showRusPreview}
							onCardCLick={() =>
								this.setState(() => {
									return { showRusPreview: null };
								})
							}
						/>
					)}
				</div>
				<form
					autoComplete="off"
					className={s.form}
					onSubmit={this.handleCardSubmitForm}
				>
					<input
						className={s.input}
						type="text"
						onChange={this.handleCardInputChange}
						name="eng"
						value={card.eng}
						placeholder="English"
					/>
					<input
						className={s.input}
						type="text"
						onChange={this.handleCardInputChange}
						name="rus"
						value={card.rus}
						placeholder="По-русски"
					/>
					<button className={s.button}>Добавить</button>
				</form>
				<div className={s.root}>
					{item.map(({ eng, rus, id }) => (
						<Card
							onDeleted={() => {
								onDeletedItem(id);
							}}
							key={id}
							eng={eng}
							rus={rus}
							id={"card-" + id}
						/>
					))}
				</div>
			</>
		);
	}
}
CardList.contextType = FirebaseContext;

export default CardList;
