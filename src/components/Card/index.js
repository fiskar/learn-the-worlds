import React from 'react';
import s from './Card.module.scss';
import cl from 'classnames';
import { CheckOutlined, DeleteOutlined } from '@ant-design/icons';

class Card extends React.Component {
    state = {
        done: false,
        isRemembered: false,
    };

    handleCardClick = () => {
        if (this.state.isRemembered) return false;
        this.setState(({ done }) => {
            return {
                done: !done,
            };
        });
    };

    handleIsRemeberClick = () => {
        this.setState(() => {
            return {
                isRemembered: true,
                done: true,
            };
        });
    };

    handleDeletedClick = () => {
        this.props.onDeleted();
    };

    render() {
        const { eng, rus, showAsDone } = this.props,
            { isRemembered } = this.state,
            showRememberCheckbox = !!this.props.onRemembered,
            showDeletedButton = !!this.props.onDeleted;
        let { done } = this.state;

        if (typeof showAsDone !== 'undefined' && null !== showAsDone) {
            done = showAsDone;
        }

        return (
            <div className={s.root}>
                <div
                    className={cl(s.card, {
                        [s.done]: done,
                        [s.isRemembered]: isRemembered,
                    })}
                    onClick={this.handleCardClick}
                >
                    <div className={s.cardInner}>
                        <div className={s.cardFront}>{eng}</div>
                        <div className={s.cardBack}>{rus}</div>
                    </div>
                </div>
                {showRememberCheckbox && (
                    <div className={s.icons}>
                        <CheckOutlined onClick={this.handleIsRemeberClick} />
                    </div>
                )}
                {showDeletedButton && (
                    <div className={s.icons}>
                        <DeleteOutlined onClick={this.handleDeletedClick} />
                    </div>
                )}
            </div>
        );
    }
}

export default Card;
