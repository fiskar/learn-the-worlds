import React from 'react';
import s from './FooterBlock.module.css';

const FooterBlock = ({ title }) => {
    return (
        <footer className={s.footer}>
            <h3>{title}</h3>
        </footer>
    );
};

export default FooterBlock;
