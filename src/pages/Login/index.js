import React, { Component } from "react";
import { Layout, Form, Input, Button, message } from "antd";
import s from "./Login.module.scss";
import FirebaseContext from "../../context/firebaseContext";
import RegisterPage from "../Register";

const { Content } = Layout;

class LoginPage extends Component {
	// state = { register: false };

	onFinish = ({ email, password }) => {
		const { signWithEmail, setUserUid } = this.context,
			{ history } = this.props;

		// this.state.register
		//     ? createUserWithEmail(email, password).catch((err) =>
		//           message.error(err.message)
		//       ) :
		signWithEmail(email, password)
			.catch((err) => message.error(err.message))
			.then((res) => {
				setUserUid(res.user.uid);
				localStorage.setItem("user", res.user.uid);
				history.push("/");
			});
		// );
	};

	onFinishFailed = (errorMsg) => {
		console.log("####: errorMsg", errorMsg);
	};

	renderForm = () => {
		const layout = {
			labelCol: { span: 6 },
			wrapperCol: { span: 18 },
		};
		const tailLayout = {
			wrapperCol: { offset: 6, span: 18 },
		};

		return (
			<Form
				{...layout}
				name="basic"
				initialValues={{ remember: true }}
				onFinish={this.onFinish}
				onFinishFailed={this.onFinishFailed}
			>
				<Form.Item
					label="Email"
					name="email"
					placeholder="email"
					rules={[
						{
							required: true,
							message: "Пожалуйста, введите Ваш email",
						},
					]}
				>
					<Input />
				</Form.Item>

				<Form.Item
					label="Пароль"
					name="password"
					rules={[
						{
							required: true,
							message: "Пожалуйста, введите Ваш пароль",
						},
					]}
				>
					<Input.Password />
				</Form.Item>

				<Form.Item {...tailLayout}>
					<Button type="primary" htmlType="submit">
						Войти
					</Button>
				</Form.Item>
				<Form.Item {...tailLayout}>
					<Button onClick={() => <RegisterPage />}>
						Зарегистрироваться
					</Button>
				</Form.Item>
			</Form>
		);
	};

	render() {
		return (
			<Layout>
				<Content>
					<div className={s.root}>
						<div className={s.form_wrap}>{this.renderForm()}</div>
					</div>
				</Content>
			</Layout>
		);
	}
}
LoginPage.contextType = FirebaseContext;

export default LoginPage;
