import React, { Component } from 'react';
import HeaderBlock from '../../components/HeaderBlock';
import FooterBlock from '../../components/FooterBlock';
import Header from '../../components/Header';
import CardList from '../../components/CardList';
import FirebaseContext from '../../context/firebaseContext';
import { LogoutOutlined } from '@ant-design/icons';
import s from './Home.module.scss';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, Spin } from 'antd';
import { fetchCardList } from '../../actions/cardListAction';
import { minusAction, plusAction } from '../../actions';

class HomePage extends Component {
    componentDidMount() {
        const { getUserCardsRef } = this.context,
            { fetchCardList } = this.props;

        fetchCardList(getUserCardsRef);

        // .catch((err) => {
        //     fetchCardListReject(err);
        // });
    }

    // handleAddCard = (card) => {
    // 	const { wordList } = this.state,
    // 		{ getUserCardsRef } = this.context,
    // 		newWordList = [...wordList, { ...card }];
    // 	getUserCardsRef().set(newWordList);
    // };

    handleSubmitButton = ({ eng, rus }) => {
        const { wordList } = this.props,
            { getUserCardsRef } = this.context;

        getUserCardsRef().set([
            ...wordList,
            {
                id: +new Date(),
                eng,
                rus,
            },
        ]);
    };

    handleDeletedItem = (id) => {
        const { wordList } = this.props,
            { getUserCardsRef } = this.context;

        const newWordList = wordList.filter((item) => item.id !== id);

        getUserCardsRef().set(newWordList);
    };

    render() {
        const {
                wordList,
                isBusy,
                countNumber,
                plusAction,
                minusAction,
            } = this.props,
            { signOut } = this.context;

        if (isBusy) {
            return <Spin />;
        }

        return (
            <>
                <HeaderBlock>
                    <Header>
                        Изучение слов
                        <div className={s.buttom_logout}>
                            <button onClick={() => signOut()}>
                                <LogoutOutlined />
                                Выйти
                            </button>
                        </div>
                    </Header>
                    <Header>
                        Добавить слово/перевод
                        <CardList
                            onDeletedItem={this.handleDeletedItem}
                            onAddCard={this.handleSubmitButton}
                            item={wordList}
                            // onSubmitButton={this.handleSubmitButton}
                        />
                    </Header>
                    <Header>{countNumber}</Header>
                    <div className={s.counter_buttons}>
                        <Button size="large" onClick={() => minusAction(1)}>
                            –
                        </Button>

                        <Button
                            shape="round"
                            danger
                            onClick={() => plusAction(1)}
                        >
                            +
                        </Button>
                    </div>
                </HeaderBlock>
                <HeaderBlock hideBackground></HeaderBlock>
                <FooterBlock title="Овальный подвал" />
            </>
        );
    }
}
HomePage.contextType = FirebaseContext;

const mapStateToProps = (state) => {
        return {
            countNumber: state.counter.count,
            isBusy: state.cardList.isBusy,
            wordList: state.cardList.payload || [],
        };
    },
    mapDispatchToProps = (dispatch) => {
        return bindActionCreators(
            {
                fetchCardList: fetchCardList,
                minusAction,
                plusAction,
            },
            dispatch
        );
    };

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
