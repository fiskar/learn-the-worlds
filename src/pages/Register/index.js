import React, { Component } from 'react';
import { Layout, Form, Input, Button } from 'antd';
import s from './Register.module.scss';
import FirebaseContext from '../../context/firebaseContext';
const { Content } = Layout;

class RegisterPage extends Component {
    onFinish = ({ email, password }) => {
        const { createUserWithEmail } = this.context;
        createUserWithEmail(email, password);
    };

    onFinishFailed = (errorMsg) => {
        console.log('####: errorMsg', errorMsg);
    };

    renderForm = () => {
        const layout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
        };
        const tailLayout = {
            wrapperCol: { offset: 6, span: 18 },
        };

        return (
            <Form
                {...layout}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
            >
                <Form.Item
                    label="Email"
                    name="email"
                    placeholder="email"
                    rules={[
                        {
                            required: true,
                            message: 'Пожалуйста, введите Ваш email',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Пароль"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Пожалуйста, введите Ваш пароль',
                        },
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Зарегистрироваться
                    </Button>
                </Form.Item>
            </Form>
        );
    };

    render() {
        return (
            <Layout>
                <Content>
                    <div className={s.root}>
                        <div className={s.form_wrap}>{this.renderForm()}</div>
                    </div>
                </Content>
            </Layout>
        );
    }
}
RegisterPage.contextType = FirebaseContext;

export default RegisterPage;
