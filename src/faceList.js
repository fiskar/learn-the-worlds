const faceList = [
    {
        face: 'https://ru.citaty.net/media/authors/albert-einstein.detail.jpg',
        name: 'Альберт Эйнштейт',
        id: 1,
    },
    {
        face: 'https://ru.citaty.net/media/authors/919_isaac-newton.detail.jpg',
        name: 'Исаак Ньютон',
        id: 2,
    },
    {
        face: 'https://ru.citaty.net/media/authors/konfucius.detail.jpg',
        name: 'Конфуций',
        id: 3,
    },
    {
        face: 'https://ru.citaty.net/media/authors/john-maynard-keynes.detail.jpg',
        name: 'Джон Мейнард Кейнс',
        id: 4,
    },
];

export default faceList;